﻿using UnityEngine;
using System.Collections;

public class CameraRotateTrigger : MonoBehaviour {

	public float rotateDegree=0;

	private CameraController mainCamera;
	// Use this for initialization
	void Start () {
		mainCamera = Camera.main.GetComponent<CameraController> ();
	}
	
	void OnTriggerEnter(Collider col){
		Debug.Log ("Entered " + col.tag);
		if (col.tag == "Player") { 

			mainCamera.RotateBy(rotateDegree);
		}
	}
}
