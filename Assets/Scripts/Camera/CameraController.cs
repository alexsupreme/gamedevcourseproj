﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject player;
	private Vector3 offset; 

	private float targetAngle = 0;
	const float rotationAmount = 1.5f;
	public float rDistance = 1.0f;
	public float rSpeed = 1.0f;
	
	// Use this for initialization
	void Start () {
		offset = transform.position-player.transform.position ;
	}
	
	// Update is called once per frame
	void Update () {
		//transform.LookAt (player.transform.position);
		transform.position = player.transform.position + offset;
		Rotate ();
	}

	public void RotateBy(float degree){
		targetAngle = degree;
	}

	void Rotate(){
		if (targetAngle != 0) {
			float step = rSpeed * Time.deltaTime;
			float orbitCircumfrance = 2F * rDistance * Mathf.PI;
			float distanceDegrees = (rSpeed / orbitCircumfrance) * 360;
			float distanceRadians = (rSpeed / orbitCircumfrance) * 2 * Mathf.PI;

			if (targetAngle > 0) {
				transform.RotateAround (player.transform.position, Vector3.up, -rotationAmount);
				targetAngle -= rotationAmount;
			} else if (targetAngle < 0) {
				transform.RotateAround (player.transform.position, Vector3.up, rotationAmount);
				targetAngle += rotationAmount;
			}

			offset = transform.position-player.transform.position ;
		}
	}
}
