﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {

    public Text health;
    public Text time;
    public Text score;

	public GameObject hud;
	public GameObject death; 
	public GameObject objective;
	public GameObject win;

	public float objectiveTTL=7f;

	public float loadMainMenueIn = 5f;
	

	public Vector3 boldTextScale = new Vector3();

    public static GUIManager i;
    void Awake() {
        i = this;
		hud.SetActive (true);
		death.SetActive (false);
		objective.SetActive (false);
		win.SetActive (false);
    }
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	  
	}

    public void AddScore(float damage)
    {
        int currentScore = int.Parse(score.text);
        currentScore += (int)damage;

        score.text = currentScore.ToString();
    }

	public void UpdateClock(float displayTime){

		if (displayTime < 30) {
			time.color = Color.red;
		}
		time.text = displayTime.ToString("#.#");
	}

	public void UpdateHealth(float health){
		if (health <= 3) {
			this.health.color = Color.red;
        }
		this.health.text = health.ToString ();
	}

	public void DisplayDeathMessage(){
		death.SetActive (true);
		hud.SetActive (false);
	}

	public void DisplayObjective(){
		StartCoroutine("DisplayObjectiveCoroutine");
	}

	IEnumerator DisplayObjectiveCoroutine(){
		hud.SetActive (false);
		objective.SetActive (true);
		yield return new WaitForSeconds(objectiveTTL);
		objective.SetActive (false);
		hud.SetActive (true);
		StopCoroutine ("DisplayObjectiveCoroutine");
	}

	public void DisplayWinMessage(){
		hud.SetActive (false);
		win.SetActive (true);
	}
}
