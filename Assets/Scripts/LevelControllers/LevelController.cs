﻿using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour {

	public int nextLevelNumber = 0;
	public float waitForNextLevel = 4f;
	public float waitForMainMenue = 4f;

	protected GameObject player;
	protected PlayerController playerController; 
	protected bool win = false;
	protected bool loose = false;
	private bool loadStarted = false;
	
	public static LevelController i;

	protected virtual void Start () {
		i=this;
		GUIManager.i.DisplayObjective();
		player = GameObject.Find ("Player");
		playerController = player.GetComponent<PlayerController> ();
	}
	
	public void SetWin(){
		win = true;
	}
	
	public void SetLost(){
		loose = true;
	}

	protected virtual void FixedUpdate(){
		if (playerController.isDead() || loose) {
			GUIManager.i.DisplayDeathMessage();
			if (!loadStarted){
				loadStarted = true;
				StartCoroutine("LoadMainMenue");
			}
		}

		if (win) {
			GUIManager.i.DisplayWinMessage();
			if (!loadStarted){
				loadStarted = true;
				StartCoroutine("LoadNextLevel");
			}
		}
		
		if (Input.GetKeyDown(KeyCode.Escape)){
			Application.LoadLevel(0);
		}
	}

	IEnumerator LoadNextLevel(){
		yield return new WaitForSeconds(waitForNextLevel);
		Application.LoadLevel(nextLevelNumber);
		StopCoroutine ("LoadNextLevel");
	}

	IEnumerator LoadMainMenue(){
		yield return new WaitForSeconds (waitForMainMenue);
		Application.LoadLevel (0);
		StopCoroutine ("LoadMainMenue");
	}
}
