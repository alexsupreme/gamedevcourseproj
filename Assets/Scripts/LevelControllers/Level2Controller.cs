﻿using UnityEngine;
using System.Collections;

public class Level2Controller : LevelController {
	
	public float svetlanaLifeCountdown=300f;
	
	protected override void Start(){
		base.Start();
	}
	
	protected override void FixedUpdate(){
		base.FixedUpdate ();
		if (!playerController.isDead()) {
			if (svetlanaLifeCountdown <= 0) {
				SetLost();
			}else{
				if (!win){
					svetlanaLifeCountdown -= Time.deltaTime;
					GUIManager.i.UpdateClock(svetlanaLifeCountdown);
				}
			}
		}
		
	}

}
