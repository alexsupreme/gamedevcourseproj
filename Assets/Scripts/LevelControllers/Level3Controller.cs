﻿using UnityEngine;
using System.Collections;

public class Level3Controller : LevelController {

	public float svetlanaLifeCountdown=200f;
	public GameObject svetlana;
	
	protected override void Start(){
		base.Start();
	}
	
	
	protected override void FixedUpdate(){
		base.FixedUpdate ();
		if (!playerController.isDead()) {
			if (svetlanaLifeCountdown <= 0) {
				svetlana.GetComponent<Animator>().SetTrigger("Loose");
				SetLost();
			}else{
				if (!win){
					svetlanaLifeCountdown -= Time.deltaTime;
                	GUIManager.i.UpdateClock(svetlanaLifeCountdown);
                }
            }
		}
		
	}
}
