﻿using UnityEngine;
using System.Collections;

public class Level1Controller : LevelController {

	public float surviveFor=300f;

	protected override void Start(){
		base.Start();
	}


	protected override void FixedUpdate(){
		base.FixedUpdate ();
		if (!playerController.isDead()) {
			if (surviveFor <= 0) {
				SetWin();
			}else{
				surviveFor -= Time.deltaTime;

                GUIManager.i.UpdateClock(surviveFor);
            }
		}

	}

}
