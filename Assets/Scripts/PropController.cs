﻿using UnityEngine;
using System.Collections;

public class PropController : MonoBehaviour {

	public float health = 3;
	public float aoeDamage = 7f;
	public float aoeRange = 5f;
	public GameObject destructedProp;
	
	void Update(){
		if (health <= 0) {
			Instantiate(destructedProp,transform.position,destructedProp.transform.rotation);
			Destroy(gameObject);
		}
	}

	void ReceiveDamage(float damage){
		health -= damage;
	}
}
