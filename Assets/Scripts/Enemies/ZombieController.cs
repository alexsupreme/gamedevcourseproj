﻿	using UnityEngine;
using System;
using System.Collections;


enum ZombieState
{
    IDLE,
    WALKING,
    RUNNING,
    ATTACKING,
    TAKE_DAMAGE,
    DEAD
};

public class ZombieController : MonoBehaviour
{
    public float damage=2f;
    
    public float health=10f;
    
    public float attackRange=5f;
	public float runningRange=20f; 
	
    public float attackReload = 2f;
    
    public GameObject persuitTarget;

	public float runningSpeed = 3f;
	public float walkingSpeed = 1f;
	
	public float dissapearAfter=3f;
	
	public AudioClip zombieHitSound;
	public AudioClip zombieBiteSound;
	public AudioClip zombieDetectSound; 
	
	public float maxAliveDistance = 70f;
	
	public GameObject blood;
	
    private ZombieState state; 
    private Animator animator;
    private AudioSource audioSource;
    
    private float currentAnimationSpeed = 0;
	private float destinationAnimationSpeed = 0 ;     
    private NavMeshAgent navMeshAgent;
    
    private bool destinationSelf = false;
    
    
    
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        state = ZombieState.WALKING;
        persuitTarget = GameObject.Find("Player");
        navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
		audioSource = GetComponent<AudioSource>();
		blood.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
		float distanceToTarget = Math.Abs((transform.position - persuitTarget.transform.position).magnitude);
		
		if (distanceToTarget > maxAliveDistance){
			Destroy(gameObject);
		}
		
		if (state != ZombieState.TAKE_DAMAGE && state != ZombieState.DEAD){			
			//Check if the zombie should run or just walk to the target 
			if (state != ZombieState.ATTACKING){
				if (distanceToTarget < runningRange){
					if (state != ZombieState.RUNNING){
						if (!audio.isPlaying){
							audioSource.clip = zombieDetectSound;
							audio.Play();
						}
					}
					state = ZombieState.RUNNING;
				}else{
					state = ZombieState.WALKING;
				}		
			}
			
			// Check if the zombie can attack
	        if (distanceToTarget <= attackRange)
	        {
	            if (state != ZombieState.ATTACKING)
	            {
	            	Debug.Log("Creating Damage Coroutine");
	            	Debug.Log("Zombie state: " + state);
	                StartCoroutine("DealDamage", persuitTarget);
					audioSource.clip = zombieBiteSound;         
	                state = ZombieState.ATTACKING;
	                
	            }
	        }
	        else
	        {
	            if (state == ZombieState.ATTACKING)
	            {
					Debug.Log("Stopping Damage Coroutine");
	                StopCoroutine("DealDamage");
	                state = ZombieState.IDLE;
	            }
	        }
	        
			Pursuit();
        }

        if (health<=0){
            state = ZombieState.DEAD;
        }

        switch (state)
        {
            case ZombieState.IDLE:
            	SetAnimationSpeed(0);
                navMeshAgent.speed = 0 ;
                break;
            case ZombieState.WALKING:
            	// 0.5 is the walking speed of the animator
            	SetAnimationSpeed(0.5f);
                navMeshAgent.speed = walkingSpeed;
                break;
            case ZombieState.RUNNING:
            	// 1.0 is the running speed in the animator 
				SetAnimationSpeed(1);
                navMeshAgent.speed = runningSpeed; 
                break;
            case ZombieState.ATTACKING:
            	// The zombie should stop when attacking
				SetAnimationSpeed(0);
				navMeshAgent.speed = 0 ; 
                animator.SetTrigger("Attack");
                break;
            case ZombieState.TAKE_DAMAGE:
				// The zombie should stop when taking damage
				SetAnimationSpeed(0);
				navMeshAgent.speed = 0 ; 
				animator.SetTrigger("Take Damage");
				audioSource.clip = zombieHitSound;
				audio.Play();				
                state = ZombieState.IDLE;
                break;
            case ZombieState.DEAD:
            	SetAnimationSpeed(0);
            	navMeshAgent.speed = 0;
                animator.SetTrigger("Death");
                navMeshAgent.enabled = false;
				StartCoroutine("StartDissapearAnimation");
                // Ignore RayCast layer
                gameObject.layer = 2;
				//StartCoroutine("StartDissapearAnimation");
                break;
        }
		
    }

    void ReceiveDamage(float damage)
    {
        state = ZombieState.TAKE_DAMAGE;
        StartCoroutine("BloodEffect");
        health -= damage;
        GUIManager.i.AddScore(damage);
		
    }
    
    IEnumerator BloodEffect() {
		blood.SetActive(true);
		yield return new WaitForSeconds(1.5f);
		blood.SetActive(false);
    }
    
    IEnumerator DealDamage(GameObject damageTarget)
    {
        yield return new WaitForSeconds(1);
		audioSource.Play();
        while (true)
        {
            damageTarget.SendMessage("ReceiveDamage", damage);
            yield return new WaitForSeconds(attackReload);
        }
        
    }
    
	void Pursuit(){
		int interactionAhead=30;
		Vector3 targetSpeed = persuitTarget.rigidbody.velocity;
		Vector3 targetFuturePosition =  persuitTarget.transform.position + ( targetSpeed * interactionAhead);
		if (navMeshAgent.enabled){
			navMeshAgent.SetDestination(targetFuturePosition);		
			destinationSelf = false;
		}
	}
	
	public void SetTarget(GameObject target){
		this.persuitTarget = target;
	}
	
	private void SetAnimationSpeed(float animSpeed){
		destinationAnimationSpeed = animSpeed;
		currentAnimationSpeed = Mathf.Lerp(currentAnimationSpeed,destinationAnimationSpeed, Time.deltaTime);
		animator.SetFloat("Speed", currentAnimationSpeed);
	}	
	
	IEnumerator StartDissapearAnimation(){
		yield return new WaitForSeconds(dissapearAfter);
		Destroy(gameObject);
	}

}
