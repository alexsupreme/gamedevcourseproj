﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour
{

    public float spawnReload = 5f;
    public GameObject zombie;
    public float minActivationDistance = 50f;

    private bool activated = false;
    private GameObject player;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("SpawnZombie");
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        float distanceFromPlayer = Vector3.Distance(transform.position, player.transform.position);

        if (distanceFromPlayer < minActivationDistance)
        {
            activated = true;
        }
        else
        {
            activated = false;
        }

    }

    IEnumerator SpawnZombie()
    {
        GameObject latestZombie;
        yield return new WaitForSeconds(Random.Range(1, 5));

        while (true)
        {
            if (activated)
            {
                latestZombie = (GameObject)Instantiate(zombie, transform.position, zombie.transform.rotation);
                latestZombie.GetComponent<ZombieController>().SetTarget(player);
            }
            yield return new WaitForSeconds(spawnReload);
        }
    }
}
