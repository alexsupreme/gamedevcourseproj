﻿using UnityEngine;
using System.Collections;

public class DestructedProp : MonoBehaviour {
	public GameObject destructionEffect;
	public float damage = 3f;
	public float hitDistance = 10f ; 
	void Start(){
		if (destructionEffect!=null) {
			Destroy(Instantiate (destructionEffect, transform.position, transform.rotation),3f);
		}
		// No need to apply force, the shards of the prop are too close together, and they "explode" naturally
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Zombie");
		foreach(GameObject enemy in enemies){
			// Check if any enemies are nearby, if so then hit them
			if ((enemy.transform.position - transform.position).magnitude < hitDistance){
				enemy.SendMessage("ReceiveDamage",damage);
			}
		}
		
		Destroy(gameObject,4f);
	}
}
