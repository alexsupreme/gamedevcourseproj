﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuButtonEvents: MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ClickButton1()
    {
        Application.LoadLevel("Lv1");
    }

    public void ClickButton2()
    {
        Application.LoadLevel("Lv2");
    }

    public void ClickButton3()
    {
        Application.LoadLevel("Lv3");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
