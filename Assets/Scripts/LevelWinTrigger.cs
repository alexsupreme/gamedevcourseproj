﻿using UnityEngine;
using System.Collections;

public class LevelWinTrigger : MonoBehaviour {

	void OnTriggerEnter(Collider col){
		if (col.tag == "Player") {
			LevelController.i.SetWin();
		}
	}
}
