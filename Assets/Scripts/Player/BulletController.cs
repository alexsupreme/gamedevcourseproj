﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour
{

    public float timeToLive = 1f;
    public float fireDistance = 15f;
    public float damage;

    public GameObject explosionEffect;

    private Vector3 firedFromPosition;
    private bool checkHit = false;

    void Update()
    {
        if (!checkHit)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, fireDistance))
            {
                if (hit.collider.tag == "Zombie" || hit.collider.tag == "Prop")
                {
					hit.collider.gameObject.SendMessage("ReceiveDamage", damage);
					if (explosionEffect && hit.collider.tag == "Zombie")
                    {
                        // Create an explosion and set it to look at the players position
                        GameObject explosion = (GameObject)Instantiate(explosionEffect, transform.position, Quaternion.identity);
                        explosion.transform.LookAt(transform.position);
                    }
                    Debug.Log("Sending damage to " + hit.collider.name);
                }
            }
            checkHit = true;
        }


        if (timeToLive <= 0)
        {
            Destroy(gameObject);
        }

        timeToLive -= Time.deltaTime;
    }

}
