﻿using UnityEngine;
using System.Collections;

enum PlayerState{
	MOVING,
	HIT,
	DEAD
};

public class PlayerController : MonoBehaviour
{

	

    //public ArrayList bullets=new ArrayList();
    public GameObject[] bullets;
    public GameObject nosslePosition;
    public float movementSpeed = 10f;
	public float health = 10f;

    private int bulletIndex;
    private Vector3 movementVelocity;
	private PlayerState playerState;
	private AudioSource audioSource;
	private Animator animator;

    void Start()
    {
        bulletIndex = 0;
        movementVelocity = new Vector3();
        playerState = PlayerState.MOVING;
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        
    }

    void Update()
    {
        GUIManager.i.UpdateHealth(health);

		// If dead, lock controlls
		if (playerState != PlayerState.DEAD){
	        float vertical = Input.GetAxis("Vertical");
	        float horizontal = Input.GetAxis("Horizontal");

        	movementVelocity = Vector3.zero;
        	movementVelocity += Camera.main.transform.right * horizontal;

        	Vector3 cameraForward = Camera.main.transform.forward;
        	cameraForward.y = 0;

       		movementVelocity += cameraForward.normalized * vertical;

	        // Move the player relative to his own local space
	        //transform.Translate(movementVelocity * movementSpeed * Time.deltaTime);
	        transform.position = transform.position + (movementVelocity * movementSpeed * Time.deltaTime);

	        RaycastHit hit;

	        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
	        {
	            if (hit.collider.tag == "Ground")
	            {

	                // Turn player to where the mouse is pointing
	                Vector3 hitPosition = hit.point;
	                hitPosition.y = transform.position.y;

	                gameObject.transform.LookAt(hitPosition);
	            }
	        }

	        if (Input.GetMouseButtonDown(0))
	        {
	            Fire(transform.rotation);
	        }
		}
		
		if (health <=0){
			playerState = PlayerState.DEAD;
		}
		
		switch (playerState){
			case PlayerState.DEAD:
				animator.SetTrigger("Death");
				break;
			case PlayerState.HIT:
				animator.SetTrigger("Hit");
				break;
			case PlayerState.MOVING:
				animator.SetFloat("Speed",movementVelocity.magnitude ) ;
				break;
		}

    }
    
    public bool isDead(){
		if (playerState == PlayerState.DEAD){
			return true;
		}
		return false;
    }

    void Fire(Quaternion direction)
    {
		audioSource.Play();
		Instantiate((GameObject)bullets[bulletIndex], nosslePosition.transform.position, direction);
    }
	
	void ReceiveDamage(float damage){
	
		if (playerState != PlayerState.DEAD) {
			health -= damage;
			Debug.Log ("Player health is : " + health);
			GUIManager.i.UpdateHealth (health);
		}
	}

}
