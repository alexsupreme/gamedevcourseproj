﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Friend : MonoBehaviour {

	public GameObject Weapon;

    public float health;
    public float speed;
    public float detectionRadius;
    public float wanderRadius;
	public float shotDamage;
    public float delayBetweenShots;

    public float followMinDistance;                 //  Closer than this to the leader and the unit will look for a new position
    public float followMaxDistance;                 //  Farther than this and the unit will look for a closer spot to the leader
    public float safeBuffer;                        //  After being pushed by another friendly unit, the unit will take this much more distance
    public float betweenFriendsDistance;            //  This is the amount of distance allowed between friendly units, less than that and they will "push" each other

	public float timeBetweenDetections = 0.5f;	    //	To avoid overflowing the game with sphere castings, do it every x miliseconds

    public GameObject nozzle;

    public Animator anim;


	bool isDead = false;
    bool foundALeader;
	Vector3 startingPoint;
	float checkSurroundingsTimer;

    bool isPushed = false;
    bool clearingPushersCache = false;
    List<Friend> pushingAllies;
    List<Friend> pushersCache;
	
	PlayerController leader;
    List<Friend> allies;
    List<ZombieController> zombies;

    NavMeshAgent navA;
    
    Vector3 lastMovementPosition;               //  Used in idle -> running animations
    float zombieShootingCountdown;              


    //  Wandering around parameters
    Vector3 destination;
    Vector3 movementDirection;
    
    AudioSource audioSource;
    

	// Use this for initialization
	void Start () {
        allies = new List<Friend>();
        pushersCache = new List<Friend>();
		zombies = new List<ZombieController>();
        pushingAllies = new List<Friend>();
		audioSource = GetComponent<AudioSource>();
		audioSource.enabled = false;

        navA = GetComponent<NavMeshAgent>();

        startingPoint = transform.position;
        FindNewDestination();
	}
	
	// Update is called once per frame
	void Update () {

        lastMovementPosition = transform.position;

		if(isDead)
			return;

        if (!isPushed && foundALeader)
        {
            FollowTheLeader();
            ShootZombie();
        }
        else
            WanderAround();

        if (isPushed)
            MoveAwayFromAllies();

        anim.speed = Vector3.Distance(transform.position, lastMovementPosition);

        DetectSurroundings();
	}

    private void ShootZombie()
    {
        zombieShootingCountdown -= Time.deltaTime;

        if (zombies.Count == 0 || zombieShootingCountdown > 0)
            return;

        zombieShootingCountdown = delayBetweenShots;

        ZombieController z = zombies[Random.Range(0, zombies.Count)];
		z.gameObject.SendMessage("ReceiveDamage", shotDamage);

        transform.LookAt(z.transform);
        Debug.DrawRay(transform.position, (z.transform.position - transform.position), Color.cyan, 15);

        Instantiate((GameObject)leader.bullets[0], nozzle.transform.position, transform.rotation);


    }

    private void DetectSurroundings()
    {
		checkSurroundingsTimer -= Time.deltaTime;

		if(checkSurroundingsTimer > 0)
			return;

		checkSurroundingsTimer = timeBetweenDetections;

        allies.Clear();
        zombies.Clear();

		Debug.DrawLine(transform.position, transform.position + transform.forward * detectionRadius);
		Collider[] hits = Physics.OverlapSphere(transform.position, detectionRadius);
        foreach (var hit in hits)
        {
   		    if (!foundALeader && hit.collider.gameObject.GetComponent<PlayerController>())
            {
				Debug.Log("FOUND A LEADER");
                foundALeader = true;
                leader = hit.collider.gameObject.GetComponent<PlayerController>();
				audioSource.enabled = true;
            }
            if (hit.collider.gameObject.transform.position != transform.position && hit.collider.gameObject.GetComponent<Friend>())
            {
                allies.Add(hit.collider.gameObject.GetComponent<Friend>());
            }
            if (hit.collider.gameObject.GetComponent<ZombieController>())
            {
                zombies.Add(hit.collider.gameObject.GetComponent<ZombieController>());
            }
        }

		//Debug.Log("Found Leader:(" + foundALeader.ToString() + "), friends:(" + allies.Count + "), enemies:(" + zombies.Count + ")");
    }

    private void WanderAround()
    {
        Vector3 nextPosition = transform.position + (speed * movementDirection) * Time.deltaTime;

        if (Vector3.Distance(nextPosition, startingPoint) > wanderRadius)
        {
            FindNewDestination();
            nextPosition = transform.position + (speed * movementDirection) * Time.deltaTime;
        }

        navA.SetDestination(nextPosition);
        
        //transform.position = nextPosition;
    }

    private void FindNewDestination()
    {
        Vector2 RandomElement = Random.insideUnitCircle * wanderRadius;
        destination = startingPoint + new Vector3(RandomElement.x, 0, RandomElement.y);

        movementDirection = destination - transform.position;
        movementDirection.Normalize();
        transform.LookAt(destination);
    }

    private void FollowTheLeader()  //  Avoid the front of the leader, head back and don't bunch up with the rest of the allies
    {
        Vector3 NewDirection = Vector3.zero;
        float distanceFromLeader = Vector3.Distance(transform.position, leader.transform.position);
        bool isInFront = IsInFrontOfLeader();

        //  If the unit is in perfect distance from the leader, there's no need for new direction
        //if (distanceFromLeader > followMinDistance && distanceFromLeader < followMaxDistance && !isInFront)
        //if(IsInComfortZone())
            //return;

        bool isOnRight = IsOnRightOfLeader();


        NewDirection = LeaderDirection();

        //  Staying away from leaders front
        if(isInFront)
            NewDirection = StayOutOfLeadersWay(NewDirection, isOnRight);
        
        //  Not getting too close to the leader
        NewDirection = KeepDistanceFromLeader(NewDirection, distanceFromLeader);

        //  move in the new direction
        NewDirection.Normalize();
        
        //transform.position = transform.position + (speed * NewDirection) * Time.deltaTime;
        navA.SetDestination(transform.position + (speed * NewDirection) * Time.deltaTime);

        //  Push other allies away from the place you reached
        if(IsInComfortZone())
            PushAlliesAway();
    }

    private void PushAlliesAway()
    {
        foreach (var ally in allies)
        {
            if (Vector3.Distance(transform.position, ally.transform.position) < betweenFriendsDistance)
                if (!pushingAllies.Contains(ally))
                    ally.PushedByAlly(this);
        }
    }

    private Vector3 KeepDistanceFromLeader(Vector3 newDirection, float distanceFromLeader)
    {
        if (distanceFromLeader < followMinDistance)
            newDirection = Vector3.Reflect(newDirection, leader.transform.right);

        return newDirection;
    }

    private Vector3 StayOutOfLeadersWay(Vector3 NewDirection, bool isOnRight)   //  move away from leader's point of view (60 degrees) 
    {
        float angle = Vector3.Angle(leader.transform.forward, NewDirection);

        if (angle > 60)
        {
            if (isOnRight)
                NewDirection += leader.transform.right;
            else
                NewDirection += leader.transform.right * (-1);
        }

        return NewDirection;
    }

    private Vector3 LeaderDirection()
    {
        return (leader.transform.position - transform.position).normalized;
    }

    private bool IsInComfortZone()  //  If the unit is within the allowed distance from the leader
    {
        float distanceFromLeader = Vector3.Distance(transform.position, leader.transform.position);

        if (distanceFromLeader > followMinDistance && distanceFromLeader < followMaxDistance)
        {
            Debug.Log(name + " is Feeling comfy");
            return true;
        }

        return false;
    }

    public void PushedByAlly(Friend ally)
    {
        if (pushingAllies.Contains(ally) || pushersCache.Contains(ally) || clearingPushersCache)
        {
            return;
        }

        pushersCache.Add(ally);
        isPushed = true;

        if (pushingAllies.Count + pushersCache.Count> 1)
            movementDirection = transform.position - ally.transform.position;
        else
            movementDirection = movementDirection + transform.position - ally.transform.position;
    }

    private void MoveAwayFromAllies()   //  Move away from those who pushed you
    {
        if (isAwayFromPushingAllies())
        {
            Debug.Log(gameObject.name + " MANAGED TO GET AWAY!");
            pushingAllies.Clear();
            pushersCache.Clear();
            isPushed = false;
            movementDirection = Vector3.zero;
        }
        else
        {
            //transform.position = transform.position + (speed * movementDirection) * Time.deltaTime;
            navA.SetDestination(transform.position + (speed * movementDirection) * Time.deltaTime);
        }
    }

    private bool isAwayFromPushingAllies()  //  Check if you're still around those who pushed you
    {
        clearingPushersCache = true;

        foreach (var ally in pushersCache)
        {
            pushingAllies.Add(ally);
        }
        clearingPushersCache = false;

        for (int i = 0; i < pushingAllies.Count; i++)
        {
            if (Vector3.Distance(transform.position, pushingAllies[i].transform.position) > betweenFriendsDistance)
                pushingAllies.Remove(pushingAllies[i]);
            else
                return false;
        }

        pushersCache.Clear();

        return true;
    }

    // UTILS

    private bool IsInFrontOfLeader()
    {
        Vector3 inFront = leader.transform.position + leader.transform.forward * 0.1f;

        if (Vector3.Distance(transform.position, leader.transform.position) > Vector3.Distance(transform.position, inFront))
            return true;

        return false;
    }

    private bool IsOnRightOfLeader()
    {
        Vector3 OnRight = leader.transform.position + leader.transform.right;

        if (Vector3.Distance(transform.position, leader.transform.position) > Vector3.Distance(transform.position, OnRight))
            return true;

        return false;
    }
}
